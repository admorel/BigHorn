#include <iostream>
#include "Analyseur.hpp"
#include <string>

using namespace std;


int main(int argc, char* argv[]){


  if (argc <= 2) {
  	cout << "Standard_SineModel Error: incorrect number of arguments." << endl;
  	cout << "Usage (at least):" << argv[0] << " audio_input output_file_sonies_marquantes output_file_frequences_longues (optional :) precision tauxPrecision seuilMarquant" << endl;
  	exit(1);
  }

	Analyseur analysor;
  string filename( argv[1] );

  //Initialisation de l'Analyseur

  analysor.monoLoader(filename);
  analysor.stereoLoader(filename);

	cout << "-------- Detection des frequences longues --------" << endl;

	analysor.detectionFrequenceLongue(argv[3]);


	cout << "-------- Detection des amplitudes marquantes --------" << endl;


	analysor.detectionAmplitudeMarquante("mono", argv[2] /*, atoi(argv[3]), atoi(argv[4]), atof(argv[5])*/);

  cout << "-------- Lecture des meta donnees --------" << endl;

  analysor.metaDataReader(filename);
  analysor.datas.affichage();

  cout << "-------- Détection des amplitudes marquantes en stéréo --------" << endl;
  analysor.detectionAmplitudeMarquante("left", "testAPI-v2-left.txt");
  analysor.detectionAmplitudeMarquante("right", "testAPI-v2-right.txt");

  analysor.stereoFileMerger("testAPI-v2-right.txt", "testAPI-v2-left.txt", "testAPI-v2-mergedFile.txt");
  //analysor.stereoFileMerger()


  //analysor.~Analyseur();

  return 0;
}
